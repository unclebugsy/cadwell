;;;; cadwell.asd

(asdf:defsystem #:cadwell
  :description "A Twitch bot"
  :author "Clint Moore <clint@ivy.io>"
  :license "Specify license here"
  :serial t
  :depends-on (
               ;; Web stuff
               #:hunchentoot
               #:hunchentoot-secure-cookie
               #:parenscript
               #:cl-who
               #:cl-ivy
               #:jsown
               #:css-lite
               
               #:drakma
               #:cl-postgres+local-time
               #:postmodern)
  :components ((:module "src"
                :components ((:file "package")
                             (:file "secrets")
                             (:file "db")
                             
                             (:file "web")
                             (:file "handlers")
                             (:module "modules"
                              :components ((:file "followbot")))))))
