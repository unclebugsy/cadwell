
(in-package :cadwell)

(define-easy-handler (follow-alert :uri "/follow/unclebugsy") ()
  (with-bare-page
      (:div
        (:img :src "//cadwell.ivy.io/cadwell.png")
        (:div :style "border:1px solid #eee;" "YOU HAVE A NEW FOLLOWER"))))
