
(in-package :cadwell)

(eval-when (:compile-toplevel :load-toplevel)
  (local-time:set-local-time-cl-postgres-readers)
  (defun symb (a b)
    (intern (format nil "~a-~a" (symbol-name a) (symbol-name b)))))

(defmacro with-pg (&body body)
  `(postmodern:with-connection
       (list "cadwell" "postgres" "fl33j0b" "127.0.0.1" :pooled-p t)
     ,@body))

(defmacro defmodel (name slot-definitions)
  `(progn
     (defclass ,name () ((uid :col-type string
                              :initform (cl-ivy:unique-id)
                              :accessor ,(symb name :uid))
                         ,@slot-definitions)      
       (:metaclass dao-class)
       (:keys uid))
     
     (with-pg
       (unless (table-exists-p ',name)
         (execute (dao-table-definition ',name))))
     
     ;; Create
     (defmacro ,(symb name 'create) (&rest args)
       `(with-pg
          (make-dao ',',name ,@args)))
     
     ;; Read
     (defun ,(symb name 'get-all) ()
       (with-pg
         (select-dao ',name)))
     
     (defun ,(symb name 'get) (id)
       (with-pg
         (get-dao ',name id)))
     
     (defmacro ,(symb name 'select) (sql-test &optional sort)
       `(with-pg
          (select-dao ',',name ,sql-test ,sort)))
     
     ;; Update
     (defun ,(symb name 'update) (,name)
       (with-pg
         (update-dao ,name)))
     
     ;; Delete
     (defun ,(symb name 'delete) (,name)
       (with-pg
           (delete-dao ,name)))))

(defmodel authentication ((person :accessor cache-person
                                    :initarg :person
                                    :col-type string)
                            (tag :accessor cache-tag
                                 :initarg :tag
                                 :col-type string)
                            (updated :accessor cache-updated-at
                                     :col-type timestamp
                                     :col-default (local-time:now))))

(defmodel person ((name :accessor person-name
                        :col-type string
                        :initarg :name)
                  (email :accessor person-email
                         :col-type string
                         :initarg :email)
                  (logo :accessor person-logo
                        :col-type string
                        :initarg :logo)
                  (temp-code :accessor person-temp-code
                                :col-type string
                                :initarg :temp-code)
                  (access-token :accessor person-access-token
                                :col-type string
                                :initarg :access-token)
                  (admin :accessor person-admin
                         :col-type boolean
                         :col-default nil)))

(defmodel follow ((user-id :accessor follow-user-id
                           :col-type string
                           :initarg :id)
                  (follows :accessor follow-follows
                           :col-type string
                           :initarg :follows)))

(defmodel cache ((twitch-id :accessor cache-twitch-id
                            :col-type string
                            :initarg :twitch-id)
                 (logo :accessor cache-twitch-logo
                       :col-type string
                       :initarg :logo)
                 (display-name :accessor twitch-display-name
                               :col-type string
                               :initarg :display-name)))

