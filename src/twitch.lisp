
(in-package :cadwell)

(setf drakma:*header-stream* *standard-output*)

(defun twitch-request (path &key token (method :get) (parameters nil))
  (multiple-value-bind (response code)
      (drakma:http-request (concatenate 'string "https://api.hitbox.tv/" path)
                           :method method
                           :parameters parameters)
    (values (jsown:parse (flexi-streams:octets-to-string response)) code)))

(define-easy-handler (authorize-callback :uri "/auth/callback") (code)
  (declare (optimize (debug 3)))
  (let ((access-token (jsown:val
                       (twitch-request "/oauth2/token"
                                       :token nil
                                       :method :post
                                       :parameters (list (cons "response_type" "code")
                                                         (cons "client_id" *twitch-client-id*)
                                                         (cons "client_secret" *twitch-client-secret*)
                                                         (cons "grant_type" "authorization_code")
                                                         (cons "redirect_uri" "http://cadwell.ivy.io/auth/callback")
                                                         (cons "code" code)))
                       "access_token")))
    (let* ((json-bundle (twitch-request "/user" :token access-token))
           (avatar (jsown:val json-bundle "logo"))
           (email (jsown:val json-bundle "email"))
           (name (jsown:val json-bundle "display_name")))
      (let ((this-person (if (person-select (:= 'email email))
                             (car (person-select (:= 'email email)))
                             (person-create :email email
                                            :logo avatar
                                            :access-token access-token
                                            :temp-code code
                                            :name name))))
        (hunchentoot-secure-cookie:set-secure-cookie "uid"
                                                     :value (person-uid this-person)
                                                     :max-age (* 60 60 24 90)
                                                     :path "/")
        (redirect "/")))))

