
(in-package :cadwell)

(setf drakma:*header-stream* *standard-output*)

(defun hitbox-noauth-request (path &key (method :get))
  (declare (optimize (debug 3)))
  (multiple-value-bind (response code)
      (drakma:http-request (concatenate 'string "https://api.hitbox.tv/" path)
                           :method method)
    (values (jsown:parse response) code)))
