
(in-package :cadwell)

(define-easy-handler (index-handler :uri "/") ()
  (with-request
      (with-page
          (htm
           (:div :class "container"
             (:div :class "jumbotron" :style "margin-top:20px;"
               (:div :class "container"
                 (:div :class "col-md-6"
                   (:h1 "Cadwell")
                   (:p :class "lead" "A hitbox.tv bot"))
                 (:div :class "col-md-6"
                   (:img :class "pull-right img-responsive" :src "/cadwell.png"))))
             (:div :class "row"
               (:div :class "col-md-6"
                 (:div :class "row")
                 (:div :class "row"
                   (:div :class "col-md-12"
                     (:div :class "panel panel-default"
                       (if (auth-person*)
                           (htm (:div :class "panel-heading" (:h3 "Welcome Back!")))
                           (htm (:div :class "panel-heading" (:h3 "Get started now!"))))
                       (:div :class "panel-body"
                         
                         (if (auth-person*)
                             (htm
                              (:div :class "row"
                                (:div :class "col-md-6"
                                  (:img :class "img-responsive" :src (person-logo (auth-person*))))
                                (:div :class "col-md-6"
                                  (:a :class "btn btn-primary" :href "/dashboard" "Dashboard."))))
                           (htm (:a
                                  :class "btn btn-primary"
                                  :href (concatenate 'string
                                                     "https://api.twitch.tv/kraken/oauth2/authorize?response_type=code&client_id=" *twitch-client-id* "&redirect_uri=http://cadwell.ivy.io/auth/callback&scope=user_read+user_blocks_edit+user_blocks_edit+channel_read+channel_editor+channel_commercial+channel_subscriptions+user_subscriptions+channel_check_subscription+chat_login")
                                  "Log in with Twitch"))))))))))))))

(define-easy-handler (dashboard :uri "/dashboard") ()
  (with-request
      (with-page
          (navbar)
          (:div :class "container"
            (:div :class "col-md-6"
              (:h3 "Followers")
              (mapcar (lambda (follow)
                        (let ((user-object (jsown:val follow "user")))
                          (htm (:li (str (jsown:val user-object "_id"))))))
                      (jsown:val
                       (twitch-request "/channels/unclebugsy/follows" :token (person-access-token (auth-person*)))
                       "follows")))))))


;; (jsown:val (jsown:val  (car (jsown:val (twitch-request "/channels/unclebugsy/follows" :token *f*) "follows")) "user")
;;            "display_name")
