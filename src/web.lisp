
(in-package :cadwell)

(defparameter *listener* nil)

(defmacro with-request (&rest body)
  (let ((person (gensym))
        (uid (gensym)))
    `(let* ((,uid (hunchentoot-secure-cookie:get-secure-cookie "uid"))
            (,person (and ,uid (person-get ,uid))))
       (let ((*authentication* (make-instance 'auth
                                              :uid ,uid
                                              :person ,person
                                              :admin (and ,person
                                                          (person-admin ,person)))))
         ,@body))))

(defmacro request-require (auth-type &rest body)
  `(if (not (funcall ,auth-type))
       (redirect "/login")
       (progn ,@body)))

(defmacro require-logged-in (&rest body)
  `(request-require #'auth-person* ,@body))

(defmacro require-admin (&rest body)
  `(request-require #'auth-admin* ,@body))

(hunchentoot-secure-cookie:set-secret-key-base
 "this is a passphrase that I like to use")

(defvar *authentication* nil)

(defclass auth ()
  ((uid :initarg :uid
        :reader auth-uid)
   (admin :initarg :admin
          :reader auth-admin)
   (person :initarg :person
           :reader auth-person)))

(defmacro navbar ()
  `(htm
    (:div :class "container" (:div :class "row" (:div :class "col-md-12" :style "margin-top:70px;")))
    (:nav :class "navbar navbar-default navbar-custom navbar-fixed-top"
      (:div :class "container-fluid"
        (:div :class "navbar-header page-scroll"
          (:button :type "button" :class "navbar-toggle" :data-toggle "collapse" :data-target "navbar-collapse"
            (:span :class "sr-only" "Toggle navigation")
            (:span :class "icon-bar")
            (:span :class "icon-bar")
            (:span :class "icon-bar"))
          (when (auth-person*)
            (htm (:a :class "navbar-brand" :href "/admin" (str (format nil "Hello, ~a" (person-email (auth-person*))))))))

        (:div :class "collapse navbar-collapse" :id "navbar-collapse"
          (:ul :class "nav navbar-nav navbar-right"
           
            (unless (string= (request-uri*) "/")
              (htm (:li (:a :href "/admin" "Home"))))

            (when (auth-admin*)
              (htm (:li (:a :href "/admin/admin" "Site Admin"))))
           
            (if (auth-person*)
                (htm (:li (:a :href "/admin/logout" "Log Out")))
                (htm (:li (:a :class "navbar-brand" :href "/admin/register" "Register"))
                     (:li (:a :href "/admin/login" "Login"))))))))))

(defun auth-person* (&optional (authentication *authentication*))
  (auth-person authentication))

(defun auth-admin* (&optional (authentication *authentication*))
  (auth-admin authentication))

(defun auth-uid* (&optional (authentication *authentication*))
  (auth-uid authentication))

(defmacro with-ajax (&rest body)
  `(progn
     (cl-who:with-html-output-to-string
         (*standard-output* nil :prologue nil :indent nil)
       (htm ,@body))))

(defmacro quick-page (&rest body)
  `(with-page
       (:div :class "container"
         (:div :class "row"
           ,@body))))

(defclass cadwell-acceptor (hunchentoot:acceptor) ())

(defmethod session-cookie-name ((acceptor cadwell-acceptor))
  "cadwell")

(defmethod acceptor-dispatch-request ((acceptor cadwell-acceptor) request)
  (loop for dispatcher in *dispatch-table*
        for action = (funcall dispatcher request)
        when action
          return (funcall action)
        finally (call-next-method)))

(setf hunchentoot:*show-lisp-errors-p* t)

(defun resource-path (path)
  (truename (asdf:system-relative-pathname (intern (package-name *PACKAGE*)) path)))

(defun set-dispatch-table ()
  (setf *dispatch-table* (list
                          #'dispatch-easy-handlers)))

(defun start-server (&key (port 8080))
  (set-dispatch-table)
  (unless *listener*
    (setq *listener*
          (make-instance 'cadwell-acceptor
                         :document-root (resource-path "static")
                         :address "127.0.0.1"
                         :port port))
    (hunchentoot:start *listener*)))

(defun stop-server ()
  (when *listener*
    (hunchentoot:stop *listener*)
    (setf *listener* nil)))

(defmacro with-bare-page (&rest body)
    `(cl-who:with-html-output-to-string
       (*standard-output* nil :prologue t :indent t)
     (htm
      (:html :lang "en"
        (:head
          (:meta :charset "utf-8")
          (:meta :name "viewport" :content "width=device-width, initial-scale=1.0")
          (:script :src "//code.jquery.com/jquery-1.11.2.min.js")
          (:link :type "text/css" :rel "stylesheet" :href "//fonts.googleapis.com/css?family=Open+Sans"))
        (:body ,@body)))))

(defmacro with-page (&rest body)
  `(cl-who:with-html-output-to-string
       (*standard-output* nil :prologue t :indent t)
     (htm
      (:html :lang "en"
        (:head
          (:meta :charset "utf-8")
          (:meta :name "viewport" :content "width=device-width, initial-scale=1.0")
          (:link :type "text/css" :rel "stylesheet" :href "//labs.ivy.io/css/bootstrap.css")
          (:link :type "text/css" :rel "stylesheet" :href "//labs.ivy.io/css/clean-blog.min.css")
          (:link :type "text/css" :rel "stylesheet" :href "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css")
          (:link :type "text/css" :rel "stylesheet" :href "//fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic")
          (:link :type "text/css" :rel "stylesheet" :href "//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800")
          (:script :src "//code.jquery.com/jquery-1.11.2.min.js")
          (:script :src "//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"))
        (:body ,@body)))))


(defun send-email (to linkback message)
  (let* ((cookies (make-instance 'drakma:cookie-jar))
         (the-json (jsown:to-json
                    (jsown:new-js
                      ("template_name" "login")
                      ("auto_text" "true")
                      ("template_content" (list
                                           (jsown:new-js
                                             ("linkback" "this will be an html snippet to link back to the site."))))
                      ("key" "ebc-4zjb9MraMY5bnxCvMw")

                      ("message" (jsown:new-js
                                   ("from_email" "deliverator@ivy.io")
                                   ("from_name" "Deliverator")
                                   ("subject" "Your Deliverator Credentials")
                                   ("global_merge_vars" (list (jsown:new-js
                                                                ("name" "headerimage")
                                                                ("content" "http://i.imgur.com/o8LMrOs.jpg"))
                                                              (jsown:new-js
                                                                ("name" "message")
                                                                ("content" message))
                                                              (jsown:new-js
                                                                ("name" "linkback")
                                                                ("content" (format nil "<a href='http://www.internet-spokesmodels.com/admin/login/authenticate?tag=~a'>Login</a>" linkback)))))
                                   ("to" (list
                                          (jsown:new-js ("email" to)))))))))
         
         (body (drakma:http-request
                "https://mandrillapp.com/api/1.0/messages/send-template.json"
                :accept "application/json"
                :external-format-in :utf-8
                :external-format-out :utf-8
                :redirect 100
                :cookie-jar cookies
                :content the-json)))
    (values (jsown:parse (flexi-streams:octets-to-string body :external-format :utf-8))
            the-json)))

(define-easy-handler (login :uri "/login") (email)
  (declare (optimize (debug 3)))
  (with-request
      (if email
          (let ((this-person (car (person-select (:= 'email email)))))
            (when this-person
              (let ((unique-tag (cl-ivy:unique-id)))
                (send-email (person-email this-person) unique-tag "You can use this link to enable any web browser.")
                (authentication-update (make-instance 'authentication
                                                      :person (person-uid this-person)
                                                      :tag unique-tag))
                (with-page
                    (navbar)
                  (:div :class "container"
                    (:div :class "row"
                      (:div :class "col-md-12 big-title" "Please check your email for a link to log in!")))))))
          (with-page
              (navbar)
            (:div :class "container top-container"
              (:div :class "row"
                (:div :class "col-md-5"
                  (:form :method "POST" :action "/admin/login"
                    (:div :class "form-group"
                      (:label :for "email" "Email Address")
                      (:input :type "email" :class "form-control" :name "email" :id "email" :placeholder "Email Address"))
                    (:div :class "form-group"
                      (:button :type "submit" :class "btn btn-primary" "Send Login Email"))))
                (:div :class "col-md-7"
                  (:p "Nope, no passwords here.  Just use your email address in the field to the left and a link will be sent via email which you can use to register any device for 90 days.")
                  (:p "Don't worry, though!  We make it easy to unregister any device with just a click."))))))))

(define-easy-handler (login-authenticate :uri "/login/authenticate") (tag)
  (with-request
      (let ((tag (car (authentication-select (:= 'tag tag)))))
        (if (not tag)
            (quick-page "Could not find that tag.")
            (let ((the-person (car (person-select (:= 'uid (auth-person tag))))))
              (hunchentoot-secure-cookie:set-secure-cookie "uid"
                                                           :value (person-uid the-person)
                                                           :max-age (* 60 60 24 90)
                                                           :path "/")
              (redirect "http://www.internet-spokesmodels.com/admin"))))))

(define-easy-handler (logout :uri "/logout") ()
  (hunchentoot-secure-cookie:delete-secure-cookie "uid")
  (redirect "http://www.internet-spokesmodels.com/admin/"))

(define-easy-handler (register-handler :uri "/register") (email email-confirm)
  (with-request
   (if (and email email-confirm)
       (progn
         (unless (string= email email-confirm)
           (redirect "http://www.internet-spokesmodels.com/admin/register/error/mismatch"))
         
         (when (person-get (:= 'email email))
           (redirect "http://www.internet-spokesmodels.com/admin/register/error/duplicate"))

         (let* ((the-person (make-instance 'person :email email :admin nil))
                (the-auth (make-instance 'authentication
                                         :person (person-uid the-person)
                                         :tag (cl-ivy:unique-id))))
           (person-update the-person)
           (authentication-update the-auth)
           (with-page
               (navbar)
               (:div :class "container"
                 (:div :class "row"
                   (:div :class "col-md-12" "Success!  You can now log in."))
                 (:div :class "row"
                   (:div :class "col-md-12" (:a :href "/admin/login"
                                              (:button :type "submit" :class "bn btn-default" "Click here to log in."))))))))
       (with-page
           (navbar)
           (:div :class "container top-container"
             (:div :class "row"
               (:div :class "col-md-5"
                 (:form :method "POST" :action "/admin/register"
                   (:div :class "form-group"
                     (:label :for "email" "Email Address")
                     (:input :type "email" :class "form-control" :name "email" :id "email" :placeholder "Email Address"))
                   (:div :class "form-group"
                     (:label :for "email-confirm" "Confirm")
                     (:input :type "email" :class "form-control" :name "email-confirm" :id "email-confirm" :placeholder "Email Address"))
                   (:div :class "form-group"
                     (:button :type "submit" :class "btn btn-default" "Register"))))
               (:div :class "col-md-7"
                 (:p "Deliverator doesn't use passwords.  Just enter your email address here, and a link will be sent to you via email that you can use to enable any browser."))))))))

(define-easy-handler (register-mismatch-error :uri "/register/error/mismatch") ()
  (with-request
      (with-page
          (navbar)
          (:div :class "container"
            (:div :class "row"
              (:div :class "col-md-12 big-title"
                (:p "Sorry!")))
            (:div :class "row"
              (:div :class "col-md-12"
                (:p "Your email addresses did not match.")))
            (:div :class "row"
              (:div :class "col-md-12"
                (:p (:a :href "/register" :class "btn btn-lg btn-default" "Please Try Again"))))))))

(define-easy-handler (register-duplicate-error :uri "/register/error/duplicate") ()
  (with-page
      (:div :class "container"
        (:div :class "row"
          (:div :class "col-md-12 big-title" "Error"))
        (:div :class "row"
          (:div :class "col-md-12"
            "There is already an account registered with that email address."))
        (:div :class "row"
          (:div :class "col-md-12"
                (:a :href "/register" (:button :class "btn btn-default" "Please Try Again")))))))

(defun send-email (to linkback message)
  (let* ((cookies (make-instance 'drakma:cookie-jar))
         (the-json (jsown:to-json
                    (jsown:new-js
                      ("template_name" "login")
                      ("auto_text" "true")
                      ("template_content" (list
                                           (jsown:new-js
                                             ("linkback" "this will be an html snippet to link back to the site."))))
                      ("key" "ebc-4zjb9MraMY5bnxCvMw")

                      ("message" (jsown:new-js
                                   ("from_email" "deliverator@ivy.io")
                                   ("from_name" "Deliverator")
                                   ("subject" "Your Deliverator Credentials")
                                   ("global_merge_vars" (list (jsown:new-js
                                                                ("name" "headerimage")
                                                                ("content" "http://i.imgur.com/o8LMrOs.jpg"))
                                                              (jsown:new-js
                                                                ("name" "message")
                                                                ("content" message))
                                                              (jsown:new-js
                                                                ("name" "linkback")
                                                                ("content" (format nil "<a href='http://www.internet-spokesmodels.com/admin/login/authenticate?tag=~a'>Login</a>" linkback)))))
                                   ("to" (list
                                          (jsown:new-js ("email" to)))))))))
         
         (body (drakma:http-request
                "https://mandrillapp.com/api/1.0/messages/send-template.json"
                :accept "application/json"
                :external-format-in :utf-8
                :external-format-out :utf-8
                :redirect 100
                :cookie-jar cookies
                :content the-json)))
    (values (jsown:parse (flexi-streams:octets-to-string body :external-format :utf-8))
            the-json)))

(define-easy-handler (site-css :uri "/css") ()
  (setf (hunchentoot:content-type*) "text/css")
  (css-lite:css
    (("*") (:font-family "Open Sans"))
    ((".warn") (:padding "9px 14px"
                :margin-top "14px;"
                :margin-bottom "14px"
                :border "1px solid #e1e1e8"
                :border-radius "4px"))
    ((".highlight") (:padding "9px 14px"
                     :margin-top "14px;"
                     :margin-bottom "14px"
                     :background-color "#f7f7f9"
                     :border "1px solid #e1e1e8"
                     :border-radius "4px"))))
